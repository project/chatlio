/*jslint browser: true*/
/*global jQuery, Drupal */

/**
 * @file
 * Attaches behaviors for chatlio and users.
 */

(function ($, Drupal, navigator) {
  'use strict';

  /**
   * chatlio behavior.
   * @type {Drupal~behavior}
   */
  Drupal.behaviors.chatlio = {
    attach: function (context, settings) {
      if (window._chatlio === undefined) {
        return;
      }
      if (settings.chatlio.disable_mobile && settings.chatlio.enabled) {
        $(document).on('chatlio.ready', function () {
          var agent = navigator.userAgent.toLowerCase();
          if (agent.match(/iP(hone|ad)|Android/i)) {
            window._chatlio.hide();
          }
        });
      }
      if (settings.chatlio.uid !== undefined) {
        $(document).on('chatlio.ready', function () {
          window._chatlio.identify('settings.chatlio.uid', {
            name: 'settings.chatlio.name',
            email: 'settings.chatlio.mail',
            loggedinas: 'settings.chatlio.loggedinas'
          });
        });
      }
    }
  };
})(jQuery, Drupal, navigator);
