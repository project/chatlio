<?php

namespace Drupal\chatlio\Form;

use Drupal\Core\Condition\ConditionManager;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformState;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Plugin\Context\ContextRepositoryInterface;
use Drupal\Core\Plugin\ContextAwarePluginInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configures Chatlio settings for this site.
 */
class ChatlioSettings extends ConfigFormBase {

  use StringTranslationTrait;

  /**
   * The condition plugin manager.
   *
   * @var \Drupal\Core\Condition\ConditionManager
   */
  protected $manager;

  /**
   * The language manager service.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $language;

  /**
   * The context repository service.
   *
   * @var \Drupal\Core\Plugin\Context\ContextRepositoryInterface
   */
  protected $contextRepository;

  /**
   * Constructs a ChatlioSettingsForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Condition\ConditionManager $manager
   *   The ConditionManager for building the visibility UI.
   * @param \Drupal\Core\Plugin\Context\ContextRepositoryInterface $contextRepository
   *   The lazy context repository service.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language
   *   The language manager.
   */
  public function __construct(ConfigFactoryInterface $configFactory, ConditionManager $manager, ContextRepositoryInterface $contextRepository, LanguageManagerInterface $language) {
    parent::__construct($configFactory);
    $this->manager = $manager;
    $this->contextRepository = $contextRepository;
    $this->language = $language;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new static(
      $container->get('config.factory'),
      $container->get('plugin.manager.condition'),
      $container->get('context.repository'),
      $container->get('language_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'chatlio_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['chatlio.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // Build visibility settings part.
    $form_state->setTemporaryValue('gathered_contexts', $this->contextRepository->getAvailableContexts());
    $form['#tree'] = TRUE;
    $form['settings'] = $this->buildSettingsInterface([], $form_state);
    $form['user'] = $this->buildUserInfoInterface([], $form_state);
    $form['visibility'] = $this->buildVisibilityInterface([], $form_state);
    return parent::buildForm($form, $form_state);
  }

  /**
   * Helper function for building the settings UI form.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The form array with the settings UI added in.
   */
  protected function buildSettingsInterface(array $form, FormStateInterface $form_state): array {
    $settings = $this->config('chatlio.settings');
    $form['settings'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Chatlio settings'),
    ];

    $form['settings']['chatlio_enable'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Chatlio'),
      '#default_value' => $settings->get('chatlio_enable', TRUE),
      '#description' => $this->t('Enable / disable Chatlio integration for this site.'),
    ];

    $form['settings']['chatlio_code'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Chatlio code'),
      '#description' => $this->t('Paste the Javascript code block from <a href="https://chatlio.com/app/#/setup">Chatlio.com</a>'),
      '#default_value' => $settings->get('chatlio_code'),
      '#attributes' => ['placeholder' => '<!-- begin chatlio code -->'],
    ];
    $form['settings']['chatlio_mobile'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Disable in mobile devices'),
      '#description' => $this->t('Hides it on mobile devices.'),
      '#default_value' => $settings->get('chatlio_mobile'),
    ];
    $form['settings']['chatlio_enable_admin'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable on admin pages.'),
      '#description' => $this->t('Embeds the Chatlio code on admin pages.'),
      '#default_value' => $settings->get('chatlio_enable_admin'),
    ];
    return $form;
  }

  /**
   * Helper function for building the user info UI form.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The form array with the visibility UI added in.
   */
  protected function buildUserInfoInterface(array $form, FormStateInterface $form_state): array {
    $settings = $this->config('chatlio.settings');
    $form['user'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('User info settings'),
    ];
    $form['user']['user_identify'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('User identify'),
      '#description' => $this->t('Enable to send information about the visitor to your site to Chatlio'),
      '#default_value' => $settings->get('user_identify'),
    ];
    $form['user']['show_user_name'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show user name in the widget'),
      '#default_value' => $settings->get('show_user_name'),
      '#states' => [
        'visible' => [
          ':input[name*=user_identify]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['user']['user_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('User name in the widget'),
      '#description' => $this->t('You can use tokens. E.g. [current-user:name].'),
      '#default_value' => $settings->get('user_name'),
      '#states' => [
        'visible' => [
          ':input[name*=user_identify]' => ['checked' => TRUE],
          ':input[name*=show_user_name]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['user']['show_user_email'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show user email in the widget'),
      '#default_value' => $settings->get('show_user_name'),
      '#states' => [
        'visible' => [
          ':input[name*=user_identify]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['user']['user_email'] = [
      '#type' => 'textfield',
      '#title' => $this->t('User email in the widget'),
      '#description' => $this->t('You can use tokens. E.g. [current-user:mail].'),
      '#default_value' => $settings->get('user_email'),
      '#states' => [
        'visible' => [
          ':input[name*=user_identify]' => ['checked' => TRUE],
          ':input[name*=show_user_email]' => ['checked' => TRUE],
        ],
      ],
    ];
    return $form;
  }

  /**
   * Helper function for building the visibility UI form.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The form array with the visibility UI added in.
   */
  protected function buildVisibilityInterface(array $form, FormStateInterface $form_state): array {
    $form['visibility_tabs'] = [
      '#type' => 'vertical_tabs',
      '#title' => $this->t('Visibility'),
      '#parents' => ['visibility_tabs'],
    ];
    $visibility = $this->config('chatlio.settings')->get('visibility');
    foreach ($this->manager->getDefinitionsForContexts($form_state->getTemporaryValue('gathered_contexts')) as $condition_id => $definition) {
      // Don't display the webform condition, causes problems.
      if ($condition_id == 'entity_bundle:webform_submission') {
        continue;
      }
      // Don't display the current theme condition.
      if ($condition_id == 'current_theme') {
        continue;
      }
      // Don't display the GTM Language condition, causes problems.
      if ($condition_id == 'gtag_language') {
        continue;
      }
      // Don't display the language condition until we have multiple languages.
      if ($condition_id == 'language' && !$this->language->isMultilingual()) {
        continue;
      }
      /** @var \Drupal\Core\Condition\ConditionInterface $condition */
      $condition = $this->manager->createInstance($condition_id, $visibility[$condition_id] ?? []);
      $form_state->set(['conditions', $condition_id], $condition);
      $condition_form = $condition->buildConfigurationForm([], $form_state);
      $condition_form['#type'] = 'details';
      $condition_form['#title'] = $condition->getPluginDefinition()['label'];
      $condition_form['#group'] = 'visibility_tabs';
      $form[$condition_id] = $condition_form;
    }

    if (isset($form['node_type'])) {
      $form['node_type']['#title'] = $this->t('Content types');
      $form['node_type']['bundles']['#title'] = $this->t('Content types');
      $form['node_type']['negate']['#type'] = 'value';
      $form['node_type']['negate']['#title_display'] = 'invisible';
      $form['node_type']['negate']['#value'] = $form['node_type']['negate']['#default_value'];
    }
    if (isset($form['user_role'])) {
      $form['user_role']['#title'] = $this->t('Roles');
      unset($form['user_role']['roles']['#description']);
      $form['user_role']['negate']['#type'] = 'value';
      $form['user_role']['negate']['#value'] = $form['user_role']['negate']['#default_value'];
    }
    if (isset($form['request_path'])) {
      $form['request_path']['#title'] = $this->t('Pages');
      $form['request_path']['negate']['#type'] = 'radios';
      $form['request_path']['negate']['#default_value'] = (int) $form['request_path']['negate']['#default_value'];
      $form['request_path']['negate']['#title_display'] = 'invisible';
      $form['request_path']['negate']['#options'] = [
        $this->t('Show for the listed pages'),
        $this->t('Hide for the listed pages'),
      ];
    }
    if (isset($form['language'])) {
      $form['language']['negate']['#type'] = 'value';
      $form['language']['negate']['#value'] = $form['language']['negate']['#default_value'];
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->submitSettings($form, $form_state);
    $this->submitUserInfo($form, $form_state);
    $this->submitVisibility($form, $form_state);
    parent::submitForm($form, $form_state);
  }

  /**
   * Helper function to independently submit the visibility UI.
   *
   * @param array $form
   *   A nested array form elements comprising the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  protected function submitSettings(array $form, FormStateInterface $form_state): void {
    foreach ($form_state->getValue('settings')['settings'] as $key => $value) {
      $this->config('chatlio.settings')->set($key, $value)->save();
    }
  }

  /**
   * Helper function to independently submit the visibility UI.
   *
   * @param array $form
   *   A nested array form elements comprising the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  protected function submitUserInfo(array $form, FormStateInterface $form_state): void {
    foreach ($form_state->getValue('user')['user'] as $key => $value) {
      $this->config('chatlio.settings')->set($key, $value)->save();
    }
  }

  /**
   * Helper function to independently submit the visibility UI.
   *
   * @param array $form
   *   A nested array form elements comprising the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  protected function submitVisibility(array $form, FormStateInterface $form_state): void {
    $visibility = [];
    foreach ($form_state->getValue('visibility') as $condition_id => $values) {
      // Allow the condition to submit the form.
      $condition = $form_state->get(['conditions', $condition_id]);
      $condition->submitConfigurationForm($form['visibility'][$condition_id], SubformState::createForSubform($form['visibility'][$condition_id], $form, $form_state));
      if ($condition instanceof ContextAwarePluginInterface) {
        $contextMapping = $values['context_mapping'] ?? [];
        $condition->setContextMapping($contextMapping);
      }

      $conditionConfiguration = $condition->getConfiguration();
      // Save the visibility conditions to config.
      $visibility[$condition_id] = $conditionConfiguration;
    }
    $this->config('chatlio.settings')->set('visibility', $visibility)->save();
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    parent::validateForm($form, $form_state);
    $this->validateVisibility($form, $form_state);
  }

  /**
   * Helper function to independently validate the visibility UI.
   *
   * @param array $form
   *   A nested array form elements comprising the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  protected function validateVisibility(array $form, FormStateInterface $form_state): void {
    // Validate visibility condition settings.
    foreach ($form_state->getValue('visibility') as $conditionId => $values) {
      // All condition plugins use 'negate' as a Boolean in their schema.
      // However, certain form elements may return it as 0/1. Cast here to
      // ensure the data is in the expected type.
      if (array_key_exists('negate', $values)) {
        $form_state->setValue(['visibility', $conditionId, 'negate'], (bool) $values['negate']);
      }
      // Allow the condition to validate the form.
      $condition = $form_state->get(['conditions', $conditionId]);
      $condition->validateConfigurationForm($form['visibility'][$conditionId], SubformState::createForSubform($form['visibility'][$conditionId], $form, $form_state));
    }
  }

}
