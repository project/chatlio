<?php

declare(strict_types = 1);

namespace Drupal\chatlio\Cache;

use Drupal\chatlio\Service\ChatlioConditionPluginsHandler;
use Drupal\Core\Cache\CacheableDependencyInterface;

/**
 * Defines the cache manager Chatlio service.
 */
class ChatlioCacheManager {

  /**
   * The condition plugin defination.
   *
   * @var \Drupal\chatlio\Service\ChatlioConditionPluginsHandler
   */
  protected $conditionsPluginsHandler;

  /**
   * Constructs the ChatlioToCacheManager.
   *
   * @param \Drupal\chatlio\Service\ChatlioConditionPluginsHandler $conditionsPluginsHandler
   *   The chatlio access controller handler.
   */
  public function __construct(ChatlioConditionPluginsHandler $conditionsPluginsHandler) {
    $this->conditionsPluginsHandler = $conditionsPluginsHandler;
  }

  /**
   * Gets cache tags based on the module settings and context plugins tags.
   *
   * @return array
   *   The cache tags.
   */
  public function getCacheTags(): array {
    $tags = ['config:chatlio.settings'];
    $conditions = $this->conditionsPluginsHandler->getConditions();
    foreach ($conditions as $condition) {
      if ($condition instanceof CacheableDependencyInterface) {
        $tags = array_merge($tags, $condition->getCacheTags());
      }
    }
    return $tags;
  }

  /**
   * Gets cache tags based on the module settings and context plugins tags.
   *
   * @return array
   *   The cache tags.
   */
  public function getCacheContexts(): array {
    $contexts = ['session'];
    $conditions = $this->conditionsPluginsHandler->getConditions();
    foreach ($conditions as $condition) {
      if ($condition instanceof CacheableDependencyInterface) {
        $contexts = array_merge($contexts, $condition->getCacheContexts());
      }
    }
    return $contexts;
  }

}
