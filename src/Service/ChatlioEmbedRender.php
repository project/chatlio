<?php

declare(strict_types = 1);

namespace Drupal\chatlio\Service;

use Drupal\Chatlio\Cache\ChatlioCacheManager;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\Routing\AdminContext;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\Core\Utility\Token;

/**
 * Defines the rendering chatlio service.
 */
class ChatlioEmbedRender {

  use StringTranslationTrait;

  /**
   * The user name.
   *
   * @var string
   */
  protected $userName;

  /**
   * The user email.
   *
   * @var string
   */
  protected $userEmail;


  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The condition plugin defination.
   *
   * @var \Drupal\chatlio\Service\ChatlioConditionPluginsHandler
   */
  protected $conditionPluginsHandler;

  /**
   * The cache manager.
   *
   * @var \Drupal\chatlio\Cache\ChatlioCacheManager
   */
  protected $cacheManager;

  /**
   * Current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * Drupal\Core\Routing\AdminContext definition.
   *
   * @var \Drupal\Core\Routing\AdminContext
   */
  protected $adminContext;

  /**
   * Constructs the ChatlioEmbedRender.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Utility\Token $token
   *   The token service.
   * @param \Drupal\chatlio\Service\ChatlioConditionPluginsHandler $conditionPluginsHandler
   *   The Chatlio access controller handler.
   * @param \Drupal\chatlio\Cache\ChatlioCacheManager $cacheManager
   *   The cache manager.
   * @param \Drupal\Core\Session\AccountInterface $currentUser
   *   Current user.
   * @param \Drupal\Core\Routing\AdminContext $admin_context
   *   The route admin context service.
   */
  public function __construct(ConfigFactoryInterface $configFactory, Token $token, ChatlioConditionPluginsHandler $conditionPluginsHandler, ChatlioCacheManager $cacheManager, AccountInterface $currentUser, AdminContext $admin_context) {
    $this->conditionPluginsHandler = $conditionPluginsHandler;
    $this->cacheManager = $cacheManager;
    $this->currentUser = $currentUser;
    $this->adminContext = $admin_context;
    $this->configFactory = $configFactory->get('chatlio.settings');
    if ($configFactory->get('chatlio.settings')->get('show_user_name')) {
      $userName = $configFactory->get('chatlio.settings')->get('user_name');
      $this->userName = $token->replace($userName, [], ['clear' => TRUE]);
    }
    if ($configFactory->get('chatlio.settings')->get('show_user_email')) {
      $userEmail = $configFactory->get('chatlio.settings')->get('user_email');
      $this->userEmail = $token->replace($userEmail, [], ['clear' => TRUE]);
    }
  }

  /**
   * Checks acess to the current requests and return renderable array or NULL.
   *
   * @return array|null
   *   The render renderable array or NULL.
   */
  public function render(): ?array {

    $settings = $this->configFactory;

    if (!$settings->get('chatlio_enable')) {
      return NULL;
    }

    // Exit if Chatlio is not allowed on admin pages and
    // we are on an admin page.
    if (!$settings->get('chatlio_enable_admin') && $this->adminContext->isAdminRoute()) {
      return NULL;
    }

    $js_settings = [
      'chatlio' => [
        'disable_mobile' => $settings->get('chatlio_mobile'),
        'enabled' => $settings->get('chatlio_enable'),
      ],
    ];

    // If user identify is enabled and the user is logged in,
    // let's get some pertinent information and pass it along as well.
    if ($settings->get('user_identify')) {
      $user = $this->currentUser;
      if ($user->id()) {
        $user_page_url = Url::fromRoute('entity.user.canonical', ['user' => $user->id()], ['absolute' => TRUE])
          ->toString();
        $js_settings['chatlio']['uid'] = $user->id();
        $js_settings['chatlio']['name'] = $this->userName ?? '';
        $js_settings['chatlio']['mail'] = $this->userEmail ?? '';
        $js_settings['chatlio']['loggedinas'] = $this->t('logged in as <a href=":url">@name</a>', [
          '@name' => $user->getAccountName(),
          ':url' => $user_page_url,
        ]);
      }
    }

    if ($this->conditionPluginsHandler->checkAccess()) {
      return [
        '#markup' => Markup::create($settings->get('chatlio_code')),
        '#attached' => [
          'library' => ['chatlio/integration'],
          'drupalSettings' => $js_settings,
        ],
        '#cache' => [
          'contexts' => $this->cacheManager->getCacheContexts(),
          'tags' => $this->cacheManager->getCacheTags(),
        ],
      ];
    }
    return NULL;
  }

}
