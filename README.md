CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
---------------------

Implementation of chatlio.com live chat for Drupal.
Usage of this module allows to select widget, that will appear on specific
site pages.

REQUIREMENTS
---------------------

 * Account on [chatlio](https://chatlio.com) service

INSTALLATION
---------------------

Download and enable as a normal module.

CONFIGURATION
-------------
1. Create and configure an account at [Chatlio](https://chatlio.com).
2. Visit https://chatlio.com/app/#/setup and copy the embed code
3. Visit admin/settings/services/chatlio on your Drupal site and
   paste the embed code into the textarea.
4. (Optional) Set up if necessary visibility settings.
   Submit the settings form.

MAINTAINERS
-----------
 * lexsoft - https://www.drupal.org/u/lexsoft
